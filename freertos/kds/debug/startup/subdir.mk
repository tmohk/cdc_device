################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Freescale/sdk2/devices/MKL82Z7/system_MKL82Z7.c 

S_UPPER_SRCS += \
C:/Freescale/sdk2/devices/MKL82Z7/gcc/startup_MKL82Z7.S 

OBJS += \
./startup/startup_MKL82Z7.o \
./startup/system_MKL82Z7.o 

C_DEPS += \
./startup/system_MKL82Z7.d 

S_UPPER_DEPS += \
./startup/startup_MKL82Z7.d 


# Each subdirectory must supply rules for building sources it contributes
startup/startup_MKL82Z7.o: C:/Freescale/sdk2/devices/MKL82Z7/gcc/startup_MKL82Z7.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -x assembler-with-cpp -DDEBUG -D__STARTUP_CLEAR_BSS -I../.. -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

startup/system_MKL82Z7.o: C:/Freescale/sdk2/devices/MKL82Z7/system_MKL82Z7.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


