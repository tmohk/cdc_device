################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/croutine.c \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/event_groups.c \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/portable/MemMang/heap_4.c \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/list.c \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/queue.c \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/tasks.c \
C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/timers.c 

OBJS += \
./freertos/croutine.o \
./freertos/event_groups.o \
./freertos/heap_4.o \
./freertos/list.o \
./freertos/queue.o \
./freertos/tasks.o \
./freertos/timers.o 

C_DEPS += \
./freertos/croutine.d \
./freertos/event_groups.d \
./freertos/heap_4.d \
./freertos/list.d \
./freertos/queue.d \
./freertos/tasks.d \
./freertos/timers.d 


# Each subdirectory must supply rules for building sources it contributes
freertos/croutine.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/croutine.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/event_groups.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/event_groups.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/heap_4.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/portable/MemMang/heap_4.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/list.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/list.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/queue.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/queue.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/tasks.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/tasks.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/timers.o: C:/Freescale/sdk2/rtos/freertos_8.2.3/Source/timers.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_FREERTOS_HEAP_SIZE=32768 -DUSB_STACK_FREERTOS -DFSL_RTOS_FREE_RTOS -DFRDM_KL82Z -DFREEDOM -I../../../../../../../rtos/freertos_8.2.3/Source/portable/GCC/ARM_CM0 -I../../../../../../../rtos/freertos_8.2.3/Source/include -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../rtos/freertos_8.2.3/Source -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


