################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Freescale/sdk2/middleware/usb_1.1.0/osa/usb_osa_bm.c 

OBJS += \
./osa/usb_osa_bm.o 

C_DEPS += \
./osa/usb_osa_bm.d 


# Each subdirectory must supply rules for building sources it contributes
osa/usb_osa_bm.o: C:/Freescale/sdk2/middleware/usb_1.1.0/osa/usb_osa_bm.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -mfloat-abi=soft -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin -Wall  -g -D_DEBUG=1 -DCPU_MKL82Z128VLK7 -DUSB_STACK_BM -DFRDM_KL82Z -DFREEDOM -I../../../../../../../CMSIS/Include -I../../../../../../../devices -I../../../../../../../devices/MKL82Z7/drivers -I../.. -I../../../../../../../devices/MKL82Z7/utilities -I../../../../../../../middleware/usb_1.1.0 -I../../../../../../../middleware/usb_1.1.0/osa -I../../../../../../../middleware/usb_1.1.0/include -I../../../../../../../middleware/usb_1.1.0/device -I../../../../.. -I../../../../../../../devices/MKL82Z7 -std=gnu99 -mapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


